import pygame
import math

WIDTH = 1200
HEIGHT = 500

GREY = (64, 64, 64)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

win = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption('Projectile Motion')


class Ball:
    def __init__(self, x, y, radius, color):
        self.x = x
        self.y = y
        self.radius = radius
        self.color = color

    def draw(self, win):
        pygame.draw.circle(win, BLACK, (self.x, self.y), self.radius)
        pygame.draw.circle(win, self.color, (self.x, self.y), self.radius - 1)

    @staticmethod
    def ball_path(start_x, start_y, power, angle, time):
        vel_x = math.cos(angle) * power
        vel_y = math.sin(angle) * power

        dist_x = vel_x * time
        dist_y = (vel_y * time) + ((-4.9 * (time)**2) / 2)

        new_x = round(dist_x + start_x)
        new_y = round(start_y - dist_y)

        return new_x, new_y


def redraw_window(win, line):
    win.fill(GREY)
    golf_ball.draw(win)
    pygame.draw.line(win, BLACK, line[0], line[1])
    pygame.display.update()


def find_angle(pos, golf_ball):
    x = golf_ball.x
    y = golf_ball.y

    mouse_x, mouse_y = pos

    try:
        angle = math.atan((y - mouse_y) / (x - mouse_x))
    except:
        angle = math.pi / 2

    if mouse_y < y and mouse_x >> x:
        angle = abs(angle)
    elif mouse_y < y and mouse_x < x:
        angle = math.pi - angle
    elif mouse_y > y and mouse_x < x:
        angle = abs(angle)
    elif mouse_y > y and mouse_x > x:
        angle = (math.pi * 2) - angle

    return angle


run = True
x = 0
y = 0
time = 0
power = 0
angle = 0
shoot = False
clock = pygame.time.Clock()
golf_ball = Ball(300, 494, 5, WHITE)


while run:
    clock.tick(200)

    if shoot and golf_ball.y < 500 - golf_ball.radius:
        time += 0.025
        new_ball_pos = Ball.ball_path(x, y, power, angle, time)
        golf_ball.x = new_ball_pos[0]
        golf_ball.y = new_ball_pos[1]
    else:
        shoot = False
        golf_ball.y = 494

    pos = pygame.mouse.get_pos()
    line = [(golf_ball.x, golf_ball.y), pygame.mouse.get_pos()]
    redraw_window(win, line)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

        if event.type == pygame.MOUSEBUTTONDOWN:
            if shoot:
                continue

            x = golf_ball.x
            y = golf_ball.y
            pos = pygame.mouse.get_pos()
            shoot = True
            power = math.sqrt((line[1][1] - line[0][1])
                              ** 2 + (line[1][0] - line[0][0]) ** 2) / 8
            angle = find_angle(pos, golf_ball)


pygame.quit()
quit()
